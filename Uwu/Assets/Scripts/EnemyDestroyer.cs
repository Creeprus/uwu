using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyDestroyer : MonoBehaviour
{
    private int requiredhits=1;
    private int hits=1;
    private double time;

    public Text ScoreText;
    private void OnMouseDown()
    {
        if (requiredhits == hits)
        {
            Destroy(gameObject);
            hits = 1;
        }
        else
        {
            hits++;
        }

    }

    private void Update()
    {
        time = EnemySpawner.timetoreturn;
   
        if (time <= 5 && time > 4.5)
        {
            requiredhits = 1;
        }
        if (time <= 4.5 && time > 3.5)
        {
            requiredhits = 2;
        }
        if (time <= 3.5 && time > 2)
        {
            requiredhits =3;
        }
        if (time <= 2 && time > 1)
        {
            requiredhits = 4;
        }
        if (time <1)
        {
            requiredhits =5;
        }

    }

}
