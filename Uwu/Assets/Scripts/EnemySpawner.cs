
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject Enemy;
    public double spawnTime;
    public static double timetoreturn;
    private double currentspawnTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentspawnTime > 0)
        {
            currentspawnTime -= Time.deltaTime;
        }
        else
        {
            SpawnEnemy();
            currentspawnTime = spawnTime;

        }
    }
    private void SpawnEnemy()
    {
        Vector3 randomPos = new Vector3(Random.Range(-13,10),10,Random.Range(-6, 16));
        var Child = Instantiate(Enemy, randomPos, Quaternion.identity);

        Child.transform.parent = gameObject.transform;
        if (spawnTime - 0.5 > 0)
        {
            spawnTime -= 0.1;
        }
        timetoreturn = spawnTime;


    }
}
