using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    GameObject Parent;

    void Start()
    {
         Parent = GameObject.Find("Enemies");
    }
    void Update()
    {
        if (Parent.transform.childCount > 10)
        {
            SceneManager.LoadScene(3);
        }
    }
    public void SendToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void NewGame()
    {
        SceneManager.LoadScene(1);

    }
    public void Credits()
    {
        SceneManager.LoadScene(2);

    }
    public void Exit()
    {
        Application.Quit();
    }

}
